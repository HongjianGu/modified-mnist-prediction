# library
# standard library

import numpy   as np
import pandas as pd
import torch
import torch.nn as nn
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset

from data import save_array

trainXPath = "/Users/parkerma/Documents/Comp/Comp 551/data/thresholded/train_x.csv"
trainYPath = "/Users/parkerma/Documents/Comp/Comp 551/data/thresholded/train_y.csv"
validXPath = "/Users/parkerma/Documents/Comp/Comp 551/data/thresholded/valid_x.csv"
validYPath = "/Users/parkerma/Documents/Comp/Comp 551/data/thresholded/valid_y.csv"
testXPath = "/Users/parkerma/Documents/Comp/Comp 551/data/thresholded/test_x.csv"


dtype = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor



class kaggleDataset(Dataset):
    def __init__(self, csv_pathX, csv_pathY, transforms=None):
        self.x_data = pd.read_csv(csv_pathX, header=None)
        self.y_data = pd.read_csv(csv_pathY, header=None).as_matrix()
        self.transforms = transforms

    def __getitem__(self, index):

        singleLable = torch.from_numpy(self.y_data[index]).type(torch.FloatTensor)
        singleX = np.asarray(self.x_data.iloc[index]).reshape(1, 64, 64)
        x_tensor = torch.from_numpy(singleX).type(dtype)
        return x_tensor, singleLable

    def __len__(self):
        return len(self.x_data.index)


class kaggleDatasetNoReshape(Dataset):
    def __init__(self, csv_pathX, csv_pathY, transforms=None):
        self.x_data = pd.read_csv(csv_pathX, header=None)
        self.y_data = pd.read_csv(csv_pathY, header=None).as_matrix()
        self.transforms = transforms

    def __getitem__(self, index):

        singleLable = torch.from_numpy(self.y_data[index]).type(torch.FloatTensor)
        singleX = np.asarray(self.x_data.iloc[index])
        x_tensor = torch.from_numpy(singleX).type(torch.FloatTensor)
        return x_tensor, singleLable

    def __len__(self):
        return len(self.x_data.index)


class testDataset(Dataset):
    def __init__(self, csv_pathX, transforms=None):
        self.x_data = pd.read_csv(csv_pathX, header=None)
        self.transforms = transforms

    def __getitem__(self, index):
        singleX = np.asarray(self.x_data.iloc[index]).reshape(1, 64, 64)
        x_tensor = torch.from_numpy(singleX).type(dtype)
        return x_tensor

    def __len__(self):
        return len(self.x_data.index)


def SVM():
    print('Loading...')
    x_train, y_train, x_valid, y_valid = pd.read_csv(trainXPath), pd.read_csv(trainYPath), pd.read_csv(validXPath), pd.read_csv(validYPath)
    print('Done')

    y_train = np.reshape(y_train, y_train.shape[0])
    y_valid = np.reshape(y_valid, y_valid.shape[0])

    x_train = x_train[:15000]
    y_train = y_train[:15000]

    clf = LinearSVC(
        C=1,
        verbose=0)

    clf.fit(x_train, y_train)
    preds = clf.predict(x_valid)

    save_array(preds, 'svm_preds')

EPOCH = 60
BATCH_SIZE = 200
LR = 0.0001

#the network class
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d( in_channels=1, out_channels=64,  kernel_size=5,  stride=1,  padding=2, ),  nn.ReLU(),  nn.BatchNorm2d(64),)
        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=64,  out_channels=64,   kernel_size=3,   stride=1, padding=1,),
            nn.ReLU(), nn.BatchNorm2d(64),nn.MaxPool2d(  kernel_size=2,  stride=2  ),  )
        self.conv3 = nn.Sequential(  nn.Conv2d( in_channels=64,  out_channels=128,  kernel_size=3,  stride=1,  padding=1,),
            nn.ReLU(),  nn.BatchNorm2d(128),nn.Dropout2d(p=0.25),)
        self.conv4 = nn.Sequential( nn.Conv2d(in_channels=128, out_channels=128,   kernel_size=3, stride=1,  padding=1,),
            nn.ReLU(),  nn.BatchNorm2d(128),nn.MaxPool2d(  kernel_size=2,  stride=2   ),  )
        self.conv5 = nn.Sequential(nn.Conv2d(in_channels=128,  kernel_size=3,  stride=1,  padding=1, ),
            nn.ReLU(),  nn.BatchNorm2d(256), nn.Dropout(0.25))
        self.conv6 = nn.Sequential( nn.Conv2d(in_channels=256,  out_channels=256,   kernel_size=3,   stride=1, padding=1,),
            nn.ReLU(),   nn.BatchNorm2d(256),nn.MaxPool2d(  kernel_size=2,  stride=2   ),  )
        self.conv7 = nn.Sequential( nn.Conv2d( in_channels=256,out_channels=512,kernel_size=3,stride=1,padding=1,),
            nn.ReLU(),nn.BatchNorm2d(512),nn.Dropout2d(p=0.25))
        self.conv8 = nn.Sequential(nn.Conv2d( in_channels=512, out_channels=512,kernel_size=3,stride=1,padding=1,),
            nn.ReLU(),nn.BatchNorm2d(512),nn.MaxPool2d( kernel_size=2,stride=2,),)
        self.conv9 = nn.Sequential(nn.Conv2d(in_channels=512,out_channels=1024,kernel_size=3,stride=1,padding=1,),
            nn.ReLU(), nn.BatchNorm2d(1024), nn.Dropout2d(p=0.25))
        self.conv10 = nn.Sequential(
            nn.Conv2d(in_channels=1024,out_channels=1024,kernel_size=3,stride=1,padding=1,),
            nn.ReLU(), nn.BatchNorm2d(1024),nn.MaxPool2d( kernel_size=2,stride=2,),)

        self.fullConnect = nn.Sequential(nn.ReLU(), nn.Dropout(p=0.5), )


        self.out = nn.Sequential( nn.Linear(1024*2*2, 10),)

    def forward(self, x):
        x = x.type(dtype).double()
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = self.conv6(x)
        x = self.conv7(x)
        x = self.conv8(x)
        x = self.conv9(x)
        x = self.conv10(x)
        x = x.view(x.size(0), -1)
        x = self.fullConnect(x)
        output = self.out(x)
        return output


def trainCNN(EPOCH, trainXPath, trainYPath, patience=8):
    print('Loading dataset')
    trainData = kaggleDataset(trainXPath, trainYPath)
    train_loader = DataLoader(dataset=trainData, batch_size=BATCH_SIZE, shuffle=True, pin_memory=False)
    cnn = CNN().cuda()
    cnn.double()
    cnn.train()
    LR = 0.03
    loss_func = nn.CrossEntropyLoss()
    for epoch in range(EPOCH):
        if (epoch % patience == 0 and epoch != 0):
            LR = LR / 10
            print('LR changed to ' + str(LR))
        optimizer = torch.optim.Adam(cnn.parameters(), lr=LR)
        for batch_idx, (data, target) in enumerate(train_loader):
            target = target.numpy()
            target = np.transpose(target)[1]
            data, target = Variable(data.type(dtype)), Variable(torch.from_numpy(target).type(dtype).long())
            output = cnn(data)
            loss = loss_func(output, target)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 50 == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    epoch, (batch_idx + 1) * len(data), len(train_loader.dataset),
                           (batch_idx + 1) / len(train_loader), loss.item()))

        if epoch % 1 == 0:
            torch.save(cnn, 'models/cnnModelGrantFinal')
            testCNNResult('models/cnnModelVINCENT_8L_augthresh', validXPath, validYPath)
    state = {
        'epoch': EPOCH,
        'state_dict': cnn.state_dict(),
        'optimizer': optimizer.state_dict()
    }
    torch.save(state, 'models/GrantFinal')
    torch.save(cnn, 'models/cnnModelGrantFinal')




def continueTrainCNN(EPOCH, trainXPath, trainYPath, modelpath,patience = 8):
    trainData = kaggleDataset(trainXPath, trainYPath)
    train_loader = DataLoader(dataset=trainData, batch_size=BATCH_SIZE,
                              shuffle=True, pin_memory=False)  # , num_workers=1,pin_memory=True)
    model = torch.load(modelpath)
    model.cpu()
    model.train()
    loss_func = nn.CrossEntropyLoss()  # the target label is not one-hotted
    LR = 0.0001
    for epoch in range(EPOCH):
        if (epoch % patience == 0 and epoch != 0):
            LR = LR / 10
        optimizer = torch.optim.Adam(model.parameters(), lr=LR)
        i = 0
        for batch_idx, (data, target) in enumerate(train_loader):
            target = target.numpy()
            target = np.transpose(target)[1]
            data, target = Variable(data.type(dtype)), Variable(torch.from_numpy(target).type(dtype).long())
            output = model(data)
            loss = loss_func(output, target)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 50 == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    epoch, (batch_idx + 1) * len(data), len(train_loader.dataset),
                           (batch_idx + 1) / len(train_loader), loss.item()))
            print(i)
            i += 1
        if epoch % 1 == 0:
            torch.save(model, modelpath)
            testCNNResult(modelpath, validXPath, validYPath)

    torch.save(model, modelpath)


def separateTrainValid():
    trainData = kaggleDatasetNoReshape(trainXPath, trainYPath)
    train_loader = DataLoader(dataset=trainData, batch_size=BATCH_SIZE, shuffle=True)
    for batch_idx, (data, target) in enumerate(train_loader):
        data = data.numpy()
        print(data.shape)
        target = target.numpy()
        print(target.shape)
        dfx = pd.DataFrame(data)
        dfy = pd.DataFrame(target)
        if (batch_idx < 20):
            with open('fastTrain_x_1.csv', 'a') as f:
                dfx.to_csv(f, index=False, header=False)
            dfy = pd.DataFrame(target)
            with open('fastTrain_y_1.csv', 'a') as f:
                dfy.to_csv(f, index=False, header=False)
        else:
            with open('train_x_1.csv', 'a') as f:
                dfx.to_csv(f, index=False, header=False)

            with open('train_y_1.csv', 'a') as f:
                dfy.to_csv(f, index=False, header=False)



def testCNN(modelName):
    testData = testDataset(testXPath)
    test_loader = DataLoader(dataset=testData, batch_size=50, shuffle=False)  # , num_workers=1,pin_memory=True)
    result = 0
    model = torch.load(modelName)
    model.cuda()
    model.eval()
    for batch_idx, data in enumerate(test_loader):
        data = Variable(data.type(dtype))

        output = model(data)
        pred = torch.max(output.cpu(), 1)[1].data.numpy()
        if batch_idx < 1:
            result = pred
        else:
            result = np.append(result, pred)
        print(len(result))
    df = pd.DataFrame(np.transpose(result.reshape(1, -1)))
    df.to_csv("submissions/test_y_result.csv", index_label='Id', header=['Label'])



def testCNNResult(modelName, ValidX, ValidY, plotCM = False):
    testData = kaggleDataset(ValidX, ValidY)
    test_loader = DataLoader(dataset=testData, batch_size=15, shuffle=False)  # , num_workers=1,pin_memory=True)
    result = 0
    trueRes = 0
    model = torch.load(modelName)
    model.cpu()
    model.eval()
    for batch_idx, (data, target) in enumerate(test_loader):
        data = Variable(data.type(dtype))
        target = np.transpose(target.cpu().numpy())[1]

        output = model(data)
        pred = torch.max(output.cpu(), 1)[1].data.numpy()
        if batch_idx < 1:
            result = pred
            trueRes = target
        else:
            result = np.append(result, pred)
            trueRes = np.append(trueRes, target)
    print('final accuracy')
    print(accuracy_score(trueRes, result))



if __name__ == '__main__':
    trainCNN(EPOCH, trainXPath, trainYPath)
    testCNNResult('models/cnnModelGrantFinal' ,validXPath, validYPath)

