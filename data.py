
import pandas as pd
import numpy as np

og_path = '/Users/parkerma/Documents/Comp/Comp 551/data/og/'


def save_array(array, fname):
    pd.DataFrame(array).to_csv(fname, header=None, index=False)


def load_array(fname):
    a = pd.read_csv(fname, header=None).values
    return a


def load_og():
    train_images = pd.read_pickle('/Users/parkerma/Documents/Comp/Comp 551/data/New_origin/train_images.pkl')
    train_labels = load_array('/Users/parkerma/Documents/Comp/Comp 551/data/New_origin/train_labels.csv')
    test_images = pd.read_pickle('/Users/parkerma/Documents/Comp/Comp 551/data/New_origin/test_images.pkl')

    test_images = test_images.reshape(-1, 4096)
    train_images = train_images.reshape(-1, 4096)

    save_array(train_images, og_path+'train_x.csv')
    save_array(train_labels, og_path+'train_y.csv')
    save_array(test_images, og_path+'test_x.csv')


# ======================================================================================================================
DATA_PATH = '/Users/parkerma/Documents/Comp/Comp 551/data/'
TRAIN_PERCENT = 0.98
THRESHOLD = 255
THRESHOLD_DIR = 'thresholded/'
MERGED = 0
LENGTHMAX = 30
DISKSIZE = 1
OUTPUTSIZE = 32


def threshold_filter(images):
    return (images >= THRESHOLD).astype(int)


def create_threshold_dataset():
    print('Reading old data')
    og_x = pd.read_csv(DATA_PATH + 'og/train_x.csv', header=None).values
    og_y = pd.read_csv(DATA_PATH + 'og/train_y.csv', header=None).values
    og_tx = pd.read_csv(DATA_PATH + 'og/test_x.csv', header=None).values

    num_train = int(TRAIN_PERCENT * og_x.shape[0])

    print('Applying filter')
    og_x = threshold_filter(og_x)
    og_tx = threshold_filter(og_tx)

    print('Shuffling')
    state = np.random.get_state()
    np.random.shuffle(og_x)
    np.random.set_state(state)
    np.random.shuffle(og_y)

    print('Splitting')
    valid_x = og_x[num_train:, :]
    valid_y = og_y[num_train:, :]

    print('Saving validation set')
    save_array(valid_x, DATA_PATH + THRESHOLD_DIR + 'valid_x.csv')
    save_array(valid_y, DATA_PATH + THRESHOLD_DIR + 'valid_y.csv')

    train_x = og_x[:num_train, :]
    train_y = og_y[:num_train, :]

    print('Saving train set')
    save_array(train_x, DATA_PATH + THRESHOLD_DIR + 'train_x.csv')
    save_array(train_y, DATA_PATH + THRESHOLD_DIR + 'train_y.csv')

    print('Saving test set')
    save_array(og_tx, DATA_PATH + THRESHOLD_DIR + 'test_x.csv')


