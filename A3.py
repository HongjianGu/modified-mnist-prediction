#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 23:46:12 2019

@author: yueyue
"""
import numpy as np
import sys
import pandas as pd
from sklearn.svm import LinearSVC
from sklearn import metrics
from sklearn.metrics import accuracy_score
# Copy
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import BernoulliNB
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.dummy import DummyClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.datasets import make_classification as mc
from sklearn.linear_model import SGDClassifier as SGD
np.set_printoptions(threshold=sys.maxsize)

class Image_Processor:
    THRESHOLD = 255
    RATIO = 0.9
    N = 0
    train_x = []
    train_y = []
    test_x = []
    test_y = []
    def load_data(self, is_threshold):
        train_images = pd.read_pickle('train_max_x')
        result = pd.read_csv("train_max_y.csv").values[:, 1]
        train_threshold_images = (train_images >= self.THRESHOLD).astype(int)
        #train_threshold_images = train_images
        self.N = len(result)
        train_len = (int)(self.N * self.RATIO)
        print(train_len)
        
        print('...Shuffling...')
        state = np.random.get_state()
        np.random.shuffle(train_threshold_images)
        np.random.set_state(state)
        np.random.shuffle(result)
        
        print('......Splitting......')
        if (is_threshold):
            self.train_x = train_threshold_images[: train_len, :, :]
            self.test_x = train_threshold_images[train_len : , :, :]
        else:
            self.train_x = train_images[: train_len, :, :]
            self.test_x = train_images[train_len : , :, :]
            
        self.train_y = result[: train_len]
        self.test_y = result[train_len : ]
        
        print('......Transforming into 2D......')
        self.train_x = self.train_x.reshape(
                self.train_x.shape[0],
                self.train_x.shape[1] * self.train_x.shape[2])
        self.test_x = self.test_x.reshape(
                self.test_x.shape[0],
                self.test_x.shape[1] * self.test_x.shape[2])
        
        print('......Done......')
        #print(train_x)
        #print(train_y)
        #print(test_x)
        #print(test_y)
        #print(train_threshold_images)
        #print(result)
    
    def SVM(self):
    
        #print(self.train_x)
        #print(self.train_y)
        #print(self.test_x)
        #print(self.test_y)
        #print("reshape")
        #print(train_x_svm.shape)
        #print(train_x_svm[:3, :])
        clf = LinearSVC(C=1, verbose=0)
    
        print("......begin fit......")
        clf.fit(self.train_x, self.train_y)
        preds = clf.predict(self.test_x)
        print("......count acc......")
        print(metrics.accuracy_score(self.test_y, preds))
        
    def multNB(self, alpha):
        #n_estimators = 10
        #model = BC( MultinomialNB(alpha = 0.5), max_samples=1.0, max_features=1.0, n_estimators=n_estimators, n_jobs = -1)
        model = MultinomialNB(alpha = alpha)
        model.fit(self.train_x[:500, :], self.train_y[:500])
        pred = model.predict(self.test_x[:500, :])
        #scores3 = cross_val_score(model, self.x_train, self.y_train, cv=5, scoring='accuracy')
        #print("Score of MultinomialNB in Cross Validation", scores3.mean() * 100)
        print(" MultinomialNB Regression : accurancy_is", metrics.accuracy_score(self.test_y[:500], pred))

    '''
    def KNN(self, iter):
        model = KNeighborsClassifier(n_neighbors =iter, weights = 'uniform', algorithm = 'auto', n_jobs = -1)
        model.fit(self.x_train, self.y_train)
        predict = model.predict(self.x_test)
        print(" kneighbors Regression : accurancy_is", metrics.accuracy_score(self.y_test, predict))
        return predict

    def NB(self):
        model =Berboulli_Naive_Bayes()
        model.train(self.x_train,self.y_train)
        model.fit(self.x_train)

    def LDA(self):
        model  = LDA()
        model.fit(self.x_train, self.y_train)
        pred = model.predict(self.x_test)

        scores3 = cross_val_score(model, self.x_train, self.y_train, cv=5, scoring='accuracy')
        print("Score of LDA in Cross Validation", scores3.mean() * 100)

        print(" LDA : accurancy_is", metrics.accuracy_score(self.y_test, pred))
        
    def SGD(self, alpha, penalty):
        model  = SGD(alpha = alpha, penalty =penalty)
        model.fit(self.x_train, self.y_train)
        pred = model.predict(self.x_test)

        #scores3 = cross_val_score(model, self.x_train, self.y_train, cv=5, scoring='accuracy')
        #print("Score of LDA in Cross Validation", scores3.mean() * 100)

        print(" SGD : accurancy_is", metrics.accuracy_score(self.y_test, pred))
        return pred

    def random_forest(self):
        clf = RandomForestClassifier(random_state=14,n_estimators=100,min_samples_leaf=2)
        clf.fit(self.x_train, self.y_train)
        y_pred = clf.predict(self.x_test)
        return y_pred
    '''

if __name__ == '__main__':
    ip = Image_Processor()
    ip.load_data(True)
    #ip.SVM()
    #ip.multNB(alpha = 0.2)
